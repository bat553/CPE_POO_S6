package controller;

import java.io.Serializable;

import nutsAndBolts.PieceSquareColor;

/**
 * @author francoise.perrin
 * 
 * Objet cr�� par le Model dans m�thode MoveCapture()
 * � destination du Controller qui en extrait les donn�es pour cr�er
 * l'objet InputViewModel � destination de la View
 * 
 */
public class OutputModelData<T> implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	public boolean isMoveDone = false;
	public T capturedPieceCoord = null;
	public T dPieceCoord = null;
	public PieceSquareColor dPieceColor = null;
	
	
	public OutputModelData(
			boolean isMoveDone, 
			T capturedPieceCoord,
			T dPieceCoord,
			PieceSquareColor dPieceColor) {
		super();
		this.isMoveDone = isMoveDone;
		this.capturedPieceCoord = capturedPieceCoord;
		this.dPieceCoord = dPieceCoord;
		this.dPieceColor = dPieceColor;
	}


	@Override
	public String toString() {
		return "DataAfterMove [isMoveDone=" + isMoveDone + ", capturedPieceIndex=" + capturedPieceCoord
				+ ", dPieceIndex=" + dPieceCoord + ", dPieceColor=" + dPieceColor + "]";
	}


	

	
}
