package model;
import java.util.LinkedList;
import java.util.List;

import nutsAndBolts.PieceSquareColor;

public abstract class AbstractPieceModel implements PieceModel {
	protected Coord coord;
	protected PieceSquareColor pieceColor;
	protected int direction;
	public AbstractPieceModel(Coord coord, PieceSquareColor pieceColor) {
		this.coord = coord;
		this.pieceColor = pieceColor;
		this.direction = PieceSquareColor.BLACK.equals(this.getPieceColor()) ? -1 : 1;
	}

	public char getColonne() {
		char colonne = ' ';
		
		colonne = this.coord.getColonne();

		return colonne;
	}


	public int getLigne() {
		int ligne = -1;
		
		ligne = this.coord.getLigne();
		
		return ligne;
	}


	public boolean hasThisCoord(Coord coord) {
		boolean hasThisCoord = false;
		
		hasThisCoord = this.coord.equals(coord);

		return hasThisCoord;
	}


	public PieceSquareColor getPieceColor() {
		PieceSquareColor color = null;
		
		color = this.pieceColor;

		return color;	
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String color = this.pieceColor.name();
		String st = "["+color.charAt(0) + "" + this.coord.toString()+"]";

		return st;
	}
	
	public List<Coord> getCoordsOnItinerary(Coord targetCoord) {

		List<Coord> coordsOnItinery = new LinkedList<Coord>(); 
		int initCol = this.getColonne();
		int initLig = this.getLigne();
		int colDistance = targetCoord.getColonne() - this.getColonne();
		int ligDistance = targetCoord.getLigne() - this.getLigne();
		int deltaLig = (int) Math.signum(ligDistance);
		int deltaCol = (int) Math.signum(colDistance);

		// Vérif déplacement en diagonale
		if (Math.abs(colDistance) == Math.abs(ligDistance)){

			// recherche coordonnées des cases traversées
			for (int i = 1; i < Math.abs(colDistance); i++) {
				Coord coord = new Coord((char) (initCol + i*deltaCol), initLig + i*deltaLig);
				coordsOnItinery.add(coord);
			}
		}
		return coordsOnItinery;
	}


	
	public void move(Coord coord) {
		if(Coord.coordonnees_valides(coord) && !coord.equals(this.coord)) {
			System.out.print("test");
			this.coord = coord;
		} 
	}
}
